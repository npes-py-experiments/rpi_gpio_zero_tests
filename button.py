from gpiozero import Button
import time

button = Button(20)

while True:
	if button.is_pressed:
		print('Button pressed')
		time.sleep(0.2)
	else:
		print('Button is not pressed')
		time.sleep(0.2)
